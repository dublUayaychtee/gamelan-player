let tempo = 160;
let vol = 60;
let playing = false;
let playCur = [0, 0]; // row, col
let synths = [];
let backwards = false;

let rows = 4;
let cols = 32;

const ModChannels = ["vol", "tmp"];
const NoteChannels = { gangsa: 1, reong: 7, gong: 4 };
const BeatChannels = { kempli: 1, kendang: 1 };

const NoteList = ["I", "O", "E", "U", "A", "1", "2", "3", "5", "6", "i"];
const NoteMap = [280, 301, 334, 415, 441, 560, 603, 668, 829, 883, 1120];

const PartTypes = {
    gangsa: {
        synth: {
            oscillator: {
                type: "triangle",
            },
            envelope: {
                attack: 0.005,
                decay: 0.3,
                sustain: 0.3,
                release: 0.3,
            },
        },
        sampleType: "single",
        samples: [false],
    },
    reong: {
        synth: {
            oscillator: {
                type: "sine",
            },
            envelope: {
                attack: 0.005,
                decay: 0.3,
                sustain: 0.3,
                release: 0.3,
            },
        },
        sampleType: "single",
        samples: [false, false, false, false, false, false, "sound/reong.wav"],
    },
    kempli: {
        sampleType: "single",
        samples: ["sound/kempli.wav"],
    },
};

let parts = [
    {
        name: "vol",
        part: "vol",
        rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
    },
    {
        name: "tmp",
        part: "tmp",
        rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
    },
    {
        name: "polos",
        part: "gangsa",
        rows: [...Array(rows)].map((e) => Array(cols).fill("1")),
    },
    {
        name: "sangsih",
        part: "gangsa",
        rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
    },
    {
        name: "kempli",
        part: "kempli",
        rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
    },
];

let cur = [0, 0, 0]; // row, part, col

console.log(rows);

function update() {
    $("#inputs").empty();
    noteBuffer = Array(parts.length).fill([]);
    console.log(noteBuffer, Object.keys(parts).length);
    for (let i = 0; i < rows; i++) {
        $("<div />", {
            id: "r" + i,
            class: "row",
        }).appendTo("#inputs");
        for (let j = 0; j < Object.keys(parts).length; j++) {
            $("<div />", {
                id: "r" + i + "p" + j,
                class: "part",
            }).appendTo("#r" + i);
            $.each(parts[j].rows[i], (colIndex, colValue) => {
                $("<div />", {
                    id: "r" + i + "p" + j + "c" + colIndex,
                    class: "cell",
                    text: colValue,
                }).appendTo("#r" + i + "p" + j);
            });
        }
    }
    $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).addClass("cur");
}

function write(key) {
    $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).text(key);
    parts[cur[1]].rows[cur[0]][cur[2]] = key;
}

function add(key) {
    $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).append(key);
    parts[cur[1]].rows[cur[0]][cur[2]] += key;
}

function next() {
    if (cur[2] < parts[cur[1]].rows[cur[0]].length - 1) {
        cur[2]++;
    } else if (cur[0] < rows - 1) {
        cur[2] = 0;
        cur[0]++;
    } else {
        cur[0] = 0;
        cur[2] = 0;
    }
}

function prev() {
    if (cur[0] == 0 && cur[2] == 0) {
        cur[0] = rows - 1;
        cur[2] = parts[cur[1]].rows[cur[0]].length - 1;
    } else if (cur[2] == 0) {
        cur[0]--;
        cur[2] = parts[cur[1]].rows[cur[0]].length - 1;
    } else {
        cur[2]--;
    }
}

function download(filename, text) {
    var element = document.createElement("a");
    element.setAttribute(
        "href",
        "data:text/plain;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function generateSynths() {
    synths = [];
    $.each(parts, (partIndex, partValue) => {
        if (partValue.part in NoteChannels || partValue.part in BeatChannels) {
            synths.push([]);
            $.each(PartTypes[partValue.part].samples, (_, sample) => {
                if (sample) {
                    if (PartTypes[partValue.part].sampleType == "single") {
                        synths[partIndex].push(
                            new Tone.Player(sample).toDestination()
                        );
                    } else {
                        synths[partIndex].push(
                            new Tone.Sampler({
                                urls: sample,
                            })
                        );
                    }
                } else {
                    synths[partIndex].push(
                        new Tone.Synth(
                            PartTypes[partValue.part].synth
                        ).toDestination()
                    );
                }
            });
        } else {
            synths.push(false);
        }
    });
}

function killSound(time) {
    $.each(parts, (partIndex, partValue) => {
        if (synths[partIndex]) {
            $.each(synths[partIndex], (_, synth) => {
                if (partValue.part in NoteChannels) {
                    synth.triggerRelease(time);
                } else if (partValue.part in BeatChannels) {
                    synth.stop(time);
                }
            });
        }
    });
}

$(document).on("keydown", function (e) {
    Tone.start();
    $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).removeClass("cur");

    if (e.altKey) {
        console.log(e.keyCode);
        switch (e.keyCode) {
            case 71:
                parts.push({
                    name: "gangsa",
                    part: "gangsa",
                    rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
                });
                update();
                break;
            case 82:
                parts.push({
                    name: "reong",
                    part: "reong",
                    rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
                });
                update();
                break;
            case 75:
                e.preventDefault();
                parts.push({
                    name: "kempli",
                    part: "kempli",
                    rows: [...Array(rows)].map((e) => Array(cols).fill(" ")),
                });
                update();
                break;
        }
    } else {
        switch (e.keyCode) {
            case 13: // enter
                if (e.shiftKey && cur[0] > 0) {
                    cur[0]--;
                } else if (!e.shiftKey && cur[0] < rows - 1) {
                    cur[0]++;
                }
                break;
            case 38: // up arrow
                if (cur[1] > 0) {
                    cur[1]--;
                }
                break;
            case 40: // down arrow
                if (cur[1] < parts.length - 1) {
                    cur[1]++;
                }
                break;
            case 37: // left arrow
                prev();
                break;
            case 39: // right arrow
                next();
                break;

            case 35:
                cur[2] = parts[cur[1]].rows[cur[0]].length - 1;
                break;
            case 36:
                if (e.shiftKey) {
                    cur = [0, 0, 0];
                } else {
                    cur[2] = 0;
                }
                break;

            case 69:
                if (e.shiftKey) {
                    download(
                        "export.json",
                        JSON.stringify({
                            parts: parts,
                            rows: rows,
                            tempo: tempo,
                        })
                    );
                }
                break;

            case 45:
                if (e.shiftKey) {
                    $.each(parts, (_, partValue) => {
                        partValue.rows.splice(
                            cur[0],
                            0,
                            new Array(cols).fill(" ")
                        );
                    });
                    rows++;
                }
                update();
                break;
            case 46:
                if (e.shiftKey) {
                    $.each(parts, (_, partValue) => {
                        partValue.rows.splice(cur[0], 1);
                    });
                    rows--;
                }
                update();
                break;
            case 220:
                if (e.shiftKey) {
                    $.each(parts, (_, partValue) => {
                        partValue.rows.push(new Array(cols).fill(" "));
                    });
                    rows++;
                }
                update();
                break;

            case 219:
                if (e.shiftKey) {
                    if (cur[0] > 0) {
                        $.each(parts, (partIndex, partValue) => {
                            [
                                partValue[cur[1]].rows[cur[0] - 1],
                                partValue[cur[1]].rows[cur[0]],
                            ] = [
                                partValue[cur[1]].rows[cur[0]],
                                partValue[cur[1]].rows[cur[0] - 1],
                            ];
                        });
                    }
                } else {
                    if (cur[1] > 0) {
                        [parts[cur[1] - 1], parts[cur[1]]] = [
                            parts[cur[1]],
                            parts[cur[1] - 1],
                        ];
                        cur[1]--;
                    }
                }
                break;
            case 221:
                if (e.shiftKey) {
                    if (cur[0] < rows) {
                        $.each(parts, (partIndex, partValue) => {
                            [
                                partValue[cur[1]].rows[cur[0] - 1],
                                partValue[cur[1]].rows[cur[0]],
                            ] = [
                                partValue[cur[1]].rows[cur[0]],
                                partValue[cur[1]].rows[cur[0] - 1],
                            ];
                        });
                    }
                } else {
                    if (cur[1] < parts.length) {
                        [parts[cur[1] - 1], parts[cur[1]]] = [
                            parts[cur[1]],
                            parts[cur[1] - 1],
                        ];
                        [synths[cur[1] - 1], synths[cur[1]]] = [
                            synths[cur[1]],
                            synths[cur[1] - 1],
                        ];
                        cur[1]--;
                    }
                }

            case 27:
                playing = !playing;
                if (e.altKey) {
                    killSound(Tone.now());
                    playing = false;
                } else {
                    if (e.shiftKey) {
                        backwards = true;
                    } else {
                        backwards = false;
                    }

                    if (playing) {
                        Tone.Transport.start();
                    }
                }
                break;
        }

        // prettier-ignore
        if (parts[cur[1]].part in NoteChannels) {
            switch (e.keyCode) {
                case 190: write("."); next(); break;
                case 32: write(" "); next(); break;
                case 49: write("O"); next(); break;
                case 50: write("E"); next(); break;
                case 51: write("U"); next(); break;
                case 52: write("A"); next(); break;
                case 53: write("1"); next(); break;
                case 54: write("2"); next(); break;
                case 55: write("3"); next(); break;
                case 56: write("5"); next(); break;
                case 57: write("6"); next(); break;
                case 48: write("i"); next(); break;
            }
        } else if (parts[cur[1]].part == 'kempli') {
            switch (e.keyCode) {
                case 32: write(" "); next(); break;
                case 88:
                    write("x");
                    next();
                    break;
            }
        } else if (ModChannels.indexOf(parts[cur[1]].part) > -1) {
            switch (e.keyCode) {
                case 173: write("-"); next(); break;
                case 32: write(" "); next(); break;
                case 48: add("0"); break;
                case 49: add("1"); break;
                case 50: add("2"); break;
                case 51: add("3"); break;
                case 52: add("4"); break;
                case 53: add("5"); break;
                case 54: add("6"); break;
                case 55: add("7"); break;
                case 56: add("8"); break;
                case 57: add("9"); break;
                case 56: add("8"); break;
                case 57: add("9"); break;
                case 188: if (e.shiftKey) {write("<"); next(); break;}
                case 190: if (e.shiftKey) {write(">"); next(); break;}
            }
        }
    }

    $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).addClass("cur");
});

update();
generateSynths();

function fileUploaded() {
    var importedFile = document.getElementById("import-file").files[0];

    var reader = new FileReader();
    reader.onload = function () {
        var fileContent = JSON.parse(reader.result);
        parts = fileContent["parts"];
        rows = fileContent["rows"];
        update();
        generateSynths();
    };
    reader.readAsText(importedFile);
}

document.getElementById("import-file").addEventListener(
    "change",
    () => {
        fileUploaded();
    },
    false
);

$(function () {
    $(".cell").on("click", function () {
        const regexp = /^r(\d+)p(\d+)c(\d+)$/;
        const id = $(this).attr("id");
        const matches = id.match(regexp);
        $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).removeClass("cur");
        for (const [index, match] of matches.entries()) {
            if (index - 1 < cur.length && index > 0) {
                cur[index - 1] = parseInt(match);
            }
        }
        $(this).addClass("cur");
    });

    Tone.Transport.scheduleRepeat((time) => {
        // use the callback time to schedule events
        $.each(parts, (partIndex, partValue) => {
            if (playing) {
                if (partValue.part in NoteChannels) {
                    if (partValue.rows[cur[0]][cur[2]] == ".") {
                        $.each(synths[partIndex], (_, synth) => {
                            synth.triggerRelease(time);
                        });
                    } else if (
                        NoteList.indexOf(partValue.rows[cur[0]][cur[2]]) > -1
                    ) {
                        synths[partIndex][0].triggerAttack(
                            NoteMap[
                                NoteList.indexOf(partValue.rows[cur[0]][cur[2]])
                            ],
                            time,
                            vol / 100
                        );
                    }
                } else if (partValue.part == "kempli") {
                    if (partValue.rows[cur[0]][cur[2]] != " ") {
                        synths[partIndex][0].start(time);
                    }
                } else if (partValue.part in ModChannels) {
                    if (partValue.part == "tmp") {
                        if (!isNaN(partValue.rows[cur[0]][cur[2]])) {
                            Tone.Transport.bpm.value = parseInt(
                                partValue.rows[cur[0]][cur[2]]
                            );
                            console.log("here");
                        }
                    } else if (partValue.part == "vol") {
                        if (!isNaN(partValue.rows[cur[0]][cur[2]])) {
                            if (0 <= partValue.rows[cur[0]][cur[2]] <= 100) {
                                vol = partValue.rows[cur[0]][cur[2]] <= 100;
                            }
                        }
                    }
                }
            } else {
                killSound(time);
                Tone.Transport.stop();
            }
        });

        $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).removeClass("cur");
        if (backwards) {
            prev();
        } else {
            next();
        }
        $("#r" + cur[0] + "p" + cur[1] + "c" + cur[2]).addClass("cur");
        console.log(cur);
    }, "16n");
});
